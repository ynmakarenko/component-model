<?php

declare(strict_types=1);

namespace LaptopRu\Component\Resource\Factory;

class Factory implements FactoryInterface
{
    private \stdClass $element;

    public function __construct(string $element)
    {
        $this->element = new $element();
    }

    public function createNew(): \stdClass
    {
        return $this->element;
    }
}
