<?php

declare(strict_types=1);

namespace LaptopRu\Component\Resource\Model;

interface TimestampableInterface extends ModelInterface
{
    public function getCreatedAt(): ?\DateTimeInterface;

    public function setCreatedAt(?\DateTimeInterface $datetime): void;

    public function getUpdatedAt(): ?\DateTimeInterface;

    public function setUpdatedAt(?\DateTimeInterface $datetime): void;
}
