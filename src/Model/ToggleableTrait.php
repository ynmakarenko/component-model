<?php

declare(strict_types=1);

namespace LaptopRu\Component\Resource\Model;

trait ToggleableTrait
{
    private bool $isEnable = true;

    public function isEnabled(): bool
    {
        return $this->isEnable;
    }

    public function setEnabled(bool $isEnable): void
    {
        $this->isEnable = $isEnable;
    }

    public function enable(): void
    {
        $this->isEnable = true;
    }

    public function disable(): void
    {
        $this->isEnable = false;
    }
}
