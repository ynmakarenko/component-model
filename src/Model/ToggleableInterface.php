<?php

declare(strict_types=1);

namespace LaptopRu\Component\Resource\Model;

interface ToggleableInterface
{
    public function isEnabled(): bool;

    public function setEnabled(bool $isEnabled): void;

    public function enable(): void;

    public function disable(): void;
}
